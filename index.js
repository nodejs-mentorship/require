const fs = require("fs");

fs.writeFileSync("data.txt", "some text");

const os = require("os");

const userInfo = os.userInfo();
console.log(userInfo.username);
console.log(`Hello ${userInfo.username}`);

const { product1, getProduct } = require("./product");

console.log(product1);
console.log(getProduct());

const lodash = require("lodash");
console.log(lodash.sum([1, 2, 3]));
