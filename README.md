# Require

## Task

1. Use npm init to initialize project named Project1.

2. Go to node.js api, find FileSystem and OS modules and check what are they used for (just
   quick overview, do no go into details)

3. Create index.js file in your project root

4. Create data.txt file and add line “some text” inside it by using fs module

5. Using OS module grab username of currently logged user and write it in console.

6. Write to file “Hello currentUser” using template strings

7. Explore what module variable is (The complete NodeJS developer course 2nd – L10)

8. Create variable in product.js file and write it to console in index.js file

```**product1** = {
"id":1,
"name":"product1",
"description":"description of product1"
}
```

9. Create function getProduct in product.js file which returns product2 details

```
{"id":2, "name":"product2", "description":"description of product2"}`
```

10. Call getProduct function in index.js and write output to console
    Require external libraries

11. In your existing application sum three numbers using lodash module and use write output to console (The complete NodeJS developer course 2nd – L11)
